# Swagger test app, Wildlfy 8, Eclipse, Swagger-UI #


To test with postman:

* GET, localhost:8080/testswagger/api/staff/0718953974

To test with Swagger : 

* http://localhost:8080/testswagger/api/swagger.json

# Run swagger-ui 

 * sudo docker run -p 81:8080 swaggerapi/swagger-ui