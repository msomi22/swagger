package ke.co.appletech.server.rest.api;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;
import ke.co.appletech.server.rest.api.resource.StaffResource;


public class APIApplication extends Application{

	HashSet<Object> singletons = new HashSet<Object>();

	public APIApplication(){
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setVersion("1.0.2");
		beanConfig.setSchemes(new String[]{"http"});
		beanConfig.setHost("localhost:8080");
		beanConfig.setBasePath("/api");
		beanConfig.setFilterClass("ke.co.appletech.server.rest.api.util.ApiAuthorizationFilterImpl");
		beanConfig.setResourcePackage("ke.co.appletech.server.rest.api.resource");
		beanConfig.setScan(true);

	}
	
	 @Override
	    public Set<Class<?>> getClasses() {
	        HashSet<Class<?>> set = new HashSet<Class<?>>();

	        set.add(StaffResource.class);
	        
	        set.add(io.swagger.jaxrs.listing.ApiListingResource.class);
	        set.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);

	        return set;
	    }
	 
	 @Override
	    public Set<Object> getSingletons() {
	        return singletons;
	    }

}
