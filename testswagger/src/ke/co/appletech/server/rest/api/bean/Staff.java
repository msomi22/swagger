/**
 * 
 */
package ke.co.appletech.server.rest.api.bean;

/**
 * @author peter
 *
 */
public class Staff {
	
	private String name;
	private String mobile;
	private String email;
	private String dob;
	private String username;
	private String password;

	
	public Staff(){
		
	}
	
	public Staff(String name) {
		this.name = name;
		mobile = "";
		email = "";
		dob = "";
		username = "";
		password = "";
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Staff [name=" + name + ", mobile=" + mobile + ", email=" + email + ", dob=" + dob + ", username="
				+ username + ", password=" + password + "]";
	}
	
	

}
