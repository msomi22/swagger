/**
 * 
 */
package ke.co.appletech.server.rest.api.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ke.co.appletech.server.rest.api.bean.Staff;
import ke.co.appletech.server.rest.api.exception.NotFoundException;
import ke.co.appletech.server.rest.api.service.StaffService;


/**
 * localhost:8080/testswagger/api/staff/{mobile}
 * localhost:8080/testswagger/api/staff/0718953974
 * 
 * localhost:8080/testswagger/api/swagger.json
 * 
 * 
 * 
 * @author peter
 *
 */

@Path("/staff")
@Api(value = "/staff", description = "Operations about staff in a school")
@Produces({"application/json", "application/xml"})
public class StaffResource {
	
	StaffService staffService = new StaffService();
	
	@GET
	  @Path("/{mobile}")
	  @ApiOperation(value = "Find a staff by mobile", 
	    notes = "Returns a staff for the given mobile."
	  )
	  @ApiResponses(value = {@ApiResponse(code = 400, message = "Invalid mobile supplied"),
	      @ApiResponse(code = 404, message = "Staff not found") })
	  public Staff getStaffById(
	      @ApiParam(value = "Mobile of staff that needs to be fetched", allowableValues = "[a mobile number]", required = true)
	      @PathParam("mobile") String mobile) throws NotFoundException {
		Staff staff = staffService.getStaffById(mobile);
	    if (null != staff) {
	      return staff;
	    } else {
	      throw new NotFoundException(404, "Staff not found");
	    }
	  }

}
